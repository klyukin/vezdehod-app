import $ from 'jquery';
import 'jquery-mask-plugin';
import Vue from 'vue';

const PhoneMaskDirective = {
    bind(el) {
        $(el).attr('type', 'tel');
        $(el).mask(
            '(900) 000-00-00',
            {
                placeholder: "(9xx) xxx-xxx",
                translation: {9: {pattern: /9/}}
            }
        );
    }
};

const MaskDirective = {
    bind(el, rel) {
        $(el).mask(rel.value);
    }
};


export default function (Vue) {
    Vue.directive('mask-phone', PhoneMaskDirective);
    Vue.directive('mask', MaskDirective);
}