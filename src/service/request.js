import UserInfo from './user-info';
import config from '../config';

const API_URL = config.apiUrl;


export class HttpError {
    constructor(code) {
        this.responseCode = code;
    }

    get errorMessage() {
        if(this.responseCode < 0)
            return 'Ошибка связи с сервером. Проверьте интернет-подключение';
        return `Сервер возвратил код ошибки ${this.responseCode}`;
    }
}


export async function request(url, {method='GET', body}={}) {
    const options = {
        method: method,
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };
    if(UserInfo.currentUser) {
        options.headers.Authorization = 'Bearer ' + UserInfo.currentUser.token;
    }
    if(body) {
        options.body = JSON.stringify(body);
    }
    try {
        const resp = await fetch(API_URL + url, options);
        if(resp.status != 200) {
            return new HttpError(resp.status);
        }
        return await resp.json();
    } catch(e) {
        return new HttpError(-1);
    }
};
