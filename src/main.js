// Import Vue
import Vue from 'vue'

// Import F7
import Framework7 from 'framework7'

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue'

// Import F7 iOS Theme Styles
//import Framework7Theme from 'framework7/dist/css/framework7.ios.min.css'
//import Framework7ThemeColors from 'framework7/dist/css/framework7.ios.colors.min.css'
// OR for Material Theme:
import Framework7Theme from 'framework7/dist/css/framework7.material.min.css'
import Framework7ThemeColors from 'framework7/dist/css/framework7.material.colors.min.css'


// Import App Custom Styles
import AppStyles from './css/app.css'
import TransStyles from './css/transitions.css'

// Import Routes
import Routes from './routes.js'

// Import App Component
import App from './app'

import 'es6-shim'
import 'whatwg-fetch'
require("font-awesome-webpack");
import Bluebird from 'bluebird';
window.Promise = Bluebird;

// Init F7 Vue Plugin
Vue.use(Framework7Vue);


import config from "./config";
import {initPushNotificatrions} from "./service/push";
import {patchF7} from "./service/f7patch";

import AppDirectives from './service/directives';
Vue.use(AppDirectives);

const EventBus = new Vue()

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function () {
            return EventBus
        }
    }
});

Vue.filter('plural', function(number, s1, s2, s5) {
    var number = parseInt(number);
    if(isNaN(number))
        return '';
    var n100 = number % 100;
    var n10 = number % 10;

    if(n10 == 0 || (n100 >= 10 && n100 <= 20) || n10 >= 5)
        return s5;
    if(n10 == 1)
        return s1;
    return s2;
});

// Init App
new Vue({
  el: '#app',
  template: '<app/>',
  // Init Framework7 by passing parameters here
  framework7: {
    root: '#app',
    /* Uncomment to enable Material theme: */
    material: true,
    routes: Routes,
    pushState: !config.isApp,
    pushStateSeparator: '',
    pushStateRoot: location.origin + '/',
	swipePanel: 'left',
    showBarsOnPageScrollEnd: false,
    init: false,
    preroute: function (view, options) {
        if(options.isBack && window.Dom7(view.activePage.container).hasClass('js-main-page')) {
            return false
        }

      return true;
  }
  },

  // Register App Component
  components: {
    app: App
  },
    mounted() {
        setPlatformClass();
    },
    methods: {
        onF7Init() {
            patchF7(window.f7);
            window.f7.init();
        }
    }
});

if(config.isApp) {
    initPushNotificatrions();
    document.addEventListener("deviceready", function() {
        try {
            setPlatformClass();
            setupBackButton();
        } catch(e) {
            alert(e);
        }
    }, false);
}

function setPlatformClass() {
    const $$ = window.Dom7;
    if(!window.device || !window.device.platform || $$('#app').length)
        return;
    $$('#app').addClass('app-' + window.device.platform.toLowerCase());

}

function setupBackButton() {
    document.addEventListener("backbutton", function() {
        try {
            if(window.Dom7(window.f7.getCurrentView().activePage.container).hasClass('js-main-page')) {
                navigator.app.exitApp();
            } else {
                window.f7.views.main.router.back();
            }
        } catch(e){}
    }, true);

}
