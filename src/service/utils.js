
export function cleanPhone(phone) {
    let result = phone.replace(/[^\d]/g, '');
    if(result.length == 11 && (result.substr(0,1) == '7' || result.substr(0,1) == '8'))
        result = result.substr(1);
    return result;
};