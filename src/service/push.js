import UserService from './user';


function init() {
    try {
        var push = PushNotification.init({android: {}});
        push.on('registration', function (data) {
            UserService.savePushToken(data.registrationId);
        });

        push.on('notification', function (data) {
            navigator.notification.alert(
                data.message,         // message
                null,                 // callback
                data.title,           // title
                'Ok'                  // buttonName
            );
        });

        push.on('error', function (e) {
            alert(e);
        });
    } catch(e) {
        alert(e);
    }
}

export function initPushNotificatrions() {
    try {
        document.addEventListener('deviceready', function () {
            init();
        }, false);
    } catch(e) {
        alert(e);
    }

}