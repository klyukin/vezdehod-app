import UserInfo from './user-info';
import {request, HttpError} from "./request";
import moment from 'moment';

var activationUserInfo = null;

export default {
    get activated() {
        return UserInfo.currentUser != null;
    },

    async activate(userInfo) {
        const resp = await request('customer/activate', {method: 'POST', body: userInfo});
        if(resp instanceof HttpError)
            return {success: false, error: resp.errorMessage};

        activationUserInfo = userInfo;
        return resp;
    },

    async confirmActivation(cardCode, code) {
        const data = {
            card_code: cardCode,
            activation_code: code
        };
        const resp = await request('customer/confirm-activation', {method: 'POST', body: data});
        if(resp.success) {
            activationUserInfo.cardCode = resp.cardCode;
            await this.saveUserActivationInfo(activationUserInfo, resp.token);
        }
        return resp;
    },
    async saveUserActivationInfo(userInfo, token) {
        UserInfo.setUserInfo(userInfo, token);
        if(UserInfo.pushToken) {
            await this._savePushToken();
        }
    },
    async login(data) {
        const resp = await request('customer/login', {method: 'POST', body: data});
        if(resp instanceof HttpError)
            return {success: false, error: resp.errorMessage};
        return resp;
    },

    async confirmLogin(cardCode, phone, code) {
        let data = {code};
        if(cardCode)
            data.card_code = cardCode;
        else
            data.phone = phone;

        const resp = await request('customer/login-confirm', {method: 'POST', body: data});
        if(resp instanceof HttpError)
            return {success: false, error: resp.errorMessage};

        if(resp.success) {
            let userInfo = resp.userInfo;
            userInfo.cardCode = userInfo.card_code;
            await this.saveUserActivationInfo(userInfo, resp.token);
        }

        return resp;
    },


    async register(userInfo) {
        const resp = await request('customer/register', {method: 'POST', body: {phone: userInfo.phone}});
        if(resp instanceof HttpError)
            return {success: false, error: resp.errorMessage};

        activationUserInfo = userInfo;
        return resp;
    },
    async confirmRegister(code) {
        const data = activationUserInfo;
        data.code = code;
        const resp = await request('customer/register-confirm', {method: 'POST', body: data});
        if(resp.success) {
            activationUserInfo.cardCode = resp.userInfo.card_code;
            await this.saveUserActivationInfo(activationUserInfo, resp.token);
        }
        return resp;
    },


    async checkCardStatus(cardCode) {
        return await request('customer/check', {method: 'POST', body: {card_code: cardCode}});
    },

    async sendCode(phone){
        const resp = await request('customer/send-code', {method: 'POST', body: phone});
        if(resp instanceof HttpError)
            return {success: false, error: resp.errorMessage};
        return resp;
    },

    async getHistoryPayment() {
        const resp = await request('customer/get-history-payment');
        return resp;
    },
    async getCustomerInfo(){
        const resp = await request('customer/get-customer-info');
        return resp;
    },

    logout() {
        UserInfo.clearUserInfo();
    },


    async editCustomerPersonalData(userInfo) {
        const data = {
            name: userInfo.name,
            birthday: userInfo.birthday,
            gender: userInfo.gender,
            city_id: userInfo.city_id,
        };
        const resp = await request('customer/edit-personal-data', {method: 'POST', body: data});
        return resp;
    },

    async sendSupport(support) {
        const data = {
            contact: support.contacts,
            message: support.message,
        };
        const resp = await request('customer/send-support', {method: 'POST', body: data});
        return resp;
    },

    async getUserStatus() {
        var status = await request('customer/status');
        if(status.bonuses) {
            status.bonuses.forEach(b => {
                b.daysLeft = moment(b.date).diff(moment(), 'days') + 1;
            });
        }
        return status;
    },
	
	get getUserInfo() {
		return UserInfo.currentUser;
    },

	async update(userInfo){
		
		const resp = await request('customer/update-notifications', {method: 'POST', body: userInfo});
		let user = UserInfo.currentUser;
		
		if(resp.success){
			user.notifications = userInfo.notifications;
			user.categs = userInfo.categs;
			UserInfo.setUserInfo(user, user.token);
		}
		
		return resp;
	},

    async savePushToken(token) {
        if(token != UserInfo.pushToken) {
            UserInfo.pushToken = token;
            if(this.activated) {
                await this._savePushToken();
            }
        }
    },

	async _savePushToken() {
	    const data = {
	        platform: device.platform,
            token: UserInfo.pushToken
        };
        await request('customer/update-push-token', {method: 'POST', body: data});
    },
}