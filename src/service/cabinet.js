import {request, HttpError} from "./request";
import moment from 'moment';

export default {
    REWARD_TYPE_PERCENT: 1,
    REWARD_TYPE_RUB: 2,
    REWARD_TYPE_BONUS: 3,

    LOTTERY_TYPE_DATE: 1,
    LOTTERY_TYPE_COUNT: 2,

    async getInfo() {
        const info = await request('cabinet/info');
        if(info instanceof HttpError)
            throw new Error(info.errorMessage, info.responseCode);
        // для удобства пропишем партнёра во все акции
        info.promoevents.forEach(function(pe) {
            pe.partner = info.partner.find(x => x.id == pe.partner_id);
        });
        if(info.lotteries) {
            info.lotteries.forEach(function (lottery) {
                if (lottery.partner_id) {
                    lottery.partner = info.partner.find(x => x.id == lottery.partner_id);
                }
                if (lottery.lottery_type_date) {
                    if (moment)
                        lottery.lottery_type_date = moment(lottery.lottery_type_date);
                }
            });
        }
        return info;
    },

    async participate(lottery) {
        const resp = await request('customer/participate-lottery', {method: 'POST', body:{lottery_id:lottery.id}});
        return resp;
    },

    async removeCoupon(couponId) {
        await request('cabinet/remove-coupon', {method: 'POST', body:{id:couponId}});
    }
}