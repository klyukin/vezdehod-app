export default [
    {
        path: '/activate/',
        component: require('./pages/activate/index.vue')
    },
    {
        path: '/activate/new-card',
        component: require('./pages/activate/new-card.vue')
    },
    {
        path: '/activate/user-info/:cardCode',
        component: require('./pages/activate/user-info.vue')
    },
    {
        path: '/activate/confirm/:mode',
        component: require('./pages/activate/confirm.vue')
    },
    {
        path: '/activate/login',
        component: require('./pages/activate/login.vue')
    },

    {
        path: '/',
        component: require('./pages/main.vue')
    },
	{
        path: '/settings/',
        component: require('./pages/settings.vue')
    },
	{
        path: '/barcode/',
        component: require('./pages/barcode.vue')
    },
    {
        path: '/bonuses/',
        component: require('./pages/bonuses.vue')
    },
    {
        path: '/history/',
        component: require('./pages/history.vue')
    },
    {
        path: '/personal_data/',
        component: require('./pages/personal_data.vue')
    },
    {
        path: '/support/',
        component: require('./pages/support.vue')
    },
    {
        path: '/map/:addresses',
        component: require('./pages/map-mobile.vue')
    },
	{
        path: '/partner/:partner',
        component: require('./pages/partner-mobile.vue')
    },
	{
        path: '/promoevent/:promoevent',
        component: require('./pages/promoevent-mobile.vue')
    },
	{
        path: '/coupon/:coupon',
        component: require('./pages/coupon.vue')
    },
	{
        path: '/info',
        component: require('./pages/info.vue')
    },
]