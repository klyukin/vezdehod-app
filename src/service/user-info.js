export default {
    currentUser: getCurrentUser(),
    _pushToken: localStorage.getItem('push_token'),

    userStatus: null,
    lottery: null,

    setUserInfo(info, token=null) {
        this.currentUser = info;
        if(token !== null)
            this.currentUser.token = token;
        localStorage.setItem('user_info', JSON.stringify(this.currentUser));
    },

    clearUserInfo() {
        this.currentUser = null;
        localStorage.removeItem('user_info');
    },

    get pushToken() {
        return this._pushToken;
    },

    set pushToken(value) {
        this._pushToken = value;
        localStorage.setItem('push_token', value);
    }
};



function getCurrentUser() {
    var s = localStorage.getItem('user_info');
    return s ? JSON.parse(s) : null;

}